const express = require('express')

// console.log(express)

const app = express()

app.get('/ping', (req, res) => {
	res.send('pong')
})

app.listen(8080)